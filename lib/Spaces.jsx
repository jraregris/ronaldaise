import Space from "./Space.jsx";
import { styled } from "uebersicht";

const StyleWrapper = styled.div``;

const Spaces = ({ raw }) => {
  let data = JSON.parse(raw);
  return (
    <StyleWrapper>
      {data.map(space => (
        <Space space={space} />
      ))}
    </StyleWrapper>
  );
};

export default Spaces;
