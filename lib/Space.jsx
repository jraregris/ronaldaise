import { styled } from "uebersicht";

const ActiveSpace = styled.span`
  padding-left: 1em;
  padding-right: 1em;
  background-color: #ccc;
  color: #0c0c0c;
`;

const InactiveSpace = styled.span`
  padding-left: 1em;
  padding-right: 1em;
`;

const Space = ({ space }) => {
  if (space.focused === 1) {
    return <ActiveSpace>{space.index}</ActiveSpace>;
  } else {
    return <InactiveSpace>{space.index}</InactiveSpace>;
  }
};

export default Space;
