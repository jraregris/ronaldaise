const yabai = "/usr/local/bin/yabai";
const jq = "/usr/local/bin/jq";

const yabai_cmd = cmd => yabai + " " + cmd;

const Yabai = {
  spaces_raw: () => yabai_cmd("-m query --spaces"),
  spaces: n =>
    yabai_cmd("-m query --displays --display " + n + " ") +
    "|" +
    jq +
    " '.spaces'",
  activeTitle: () =>
    yabai + " -m query --windows --window" + "|" + jq + " '.title'",
  spaces_with_active: n =>
    yabai_cmd("-m query --spaces --display " + n + " ") +
    "|" +
    jq +
    " '[.[]|{index,focused}]'"
};

export default Yabai;
