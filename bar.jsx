import { styled } from "uebersicht";

export const refreshFrequency = false;

export const initialState = "...";
export const render = output => <div />;

export const className = {
  zIndex: "-1",
  top: "0px",
  left: "0px",
  boxSizing: "border-box",
  width: "100%",
  height: "23px",
  borderBottom: "1px solid #1c1c1c",
  backgroundColor: "#0c0c0c",
  opacity: 0.9
};
