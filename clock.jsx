export const command = "date +'%a %d %b – %H:%M:%S'";
export const refreshFrequency = 1000;
export const initialState = "...";

export const updateState = event => {
  if (event.error) return event.error;
  return event.output;
};

export const render = output => <div>{output}</div>;

export const className = {
  top: "0px",
  right: "0px",
  boxSizing: "border-box",
  padding: "7px 12px 6px 12px",
  fontFamily: "Fira Code",
  fontWeight: "400",
  fontSize: "14px",
  lineHeight: "14px",
  color: "#cccccc",
  textAlign: "center"
};
