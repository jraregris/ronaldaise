import Yabai from "./lib/yabai";
import { styled } from "uebersicht";

export const command = Yabai.spaces_raw();

export const refreshFrequency = 100;
export const initialState = "...";

export const updateState = event => {
  if (event.error) return String(event.error);
  return event.output;
};

export const render = output => {
  if (output === "") {
    return "?";
  }

  let spaces = "...";

  try {
    spaces = JSON.parse(output);
  } catch (e) {
    return <div>{e.message}</div>;
  }
  return (
    <div>
      {spaces.map(space => (
        <Space space={space} />
      ))}
    </div>
  );
};

const FocusedSpace = styled.span`
  background-color: #ccc;
  padding: 0 0.5em;
  color: #000;
`;
const UnfocusedSpace = styled.span`
  padding: 0 0.5em;
`;

const Space = ({ space }) => {
  switch (space.focused) {
    case 1:
      return <FocusedSpace>{space.index}</FocusedSpace>;
    default:
      return <UnfocusedSpace>{space.index}</UnfocusedSpace>;
  }
};

export const className = {
  top: "0px",
  boxSizing: "border-box",
  padding: "7px 12px 6px 12px",
  fontFamily: "Fira Code",
  fontWeight: "400",
  fontSize: "14px",
  lineHeight: "14px",
  color: "#cccccc",
  textAlign: "center"
};
