import Yabai from "./lib/yabai";

export const command = Yabai.activeTitle();
export const refreshFrequency = 500;
export const initialState = "...";

export const updateState = event => {
  if (event.error) return String(event.error);
  return event.output;
};

export const render = output => {
  const sliced = output.slice(1, -2);
  return <div>{sliced}</div>;
};

export const className = {
  top: "0px",
  left: "600px",
  boxSizing: "border-box",
  padding: "7px 12px 6px 12px",
  fontFamily: "Fira Code",
  fontWeight: "400",
  fontSize: "14px",
  lineHeight: "14px",
  color: "#cccccc",
  textAlign: "center"
};
