import Yabai from "./lib/yabai";
import Spaces from "./lib/Spaces.jsx";

export const command = Yabai.spaces_with_active(1);

export const refreshFrequency = 500;
export const initialState = "...";

export const updateState = event => {
  if (event.error) return String(event.error);
  return event.output;
};

export const render = output => <Spaces raw={output} />;

export const className = {
  top: "0px",
  boxSizing: "border-box",
  padding: "7px 12px 6px 12px",
  fontFamily: "Fira Code",
  fontWeight: "400",
  fontSize: "14px",
  lineHeight: "14px",
  color: "#cccccc",
  textAlign: "center"
};
